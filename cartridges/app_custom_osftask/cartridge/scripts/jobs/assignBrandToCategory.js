'use strict';

var Status = require('dw/system/Status');
var Logger = require('dw/system/Logger');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var FileWriter = require('dw/io/FileWriter');
var XmlStreamWriter = require('dw/io/XMLStreamWriter');
var File = require('dw/io/File');
var Site = require('dw/system/Site');
var CatalogMgr = require('dw/catalog/CatalogMgr');

/**
 * searches for products with specified brand attribute value
 * @param {string} brandName - brand value
 * @returns {dw.util.List} list objects with product hits
 */
function getProducts(brandName) {
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');

    var psm = new ProductSearchModel();
    psm.setCategoryID('root');
    psm.orderableProductsOnly = false;
    psm.recursiveCategorySearch = true;
    psm.addRefinementValues('brand', brandName);
    psm.search();

    return psm.getProductSearchHits().asList();
}

/**
 * creates catalog xml file for import based on products list and category id
 * @param {dw.util.List} products - product hits list
 * @param {string} categoryID - category ID
 * @param {string} workingFolder - folder the file to be saved in
 * @returns {dw.system.Status} status
 */
function createCategoryAssignmentFile(products, categoryID, workingFolder) {
    try {
        var productsIterator = products.iterator();
        var siteCatalogID = CatalogMgr.getSiteCatalog().getID();
        var siteID = Site.getCurrent().ID;
        var timeStamp = '_' + StringUtils.formatCalendar(new Calendar(), 'yyyMMddHHmmss');
        var fileXMLName = workingFolder + File.SEPARATOR + 'brandToCategoryAssign_' + siteID + timeStamp + '.xml';
        var fileXML = new File(File.IMPEX + File.SEPARATOR + fileXMLName);
        if (!fileXML.exists()) {
            fileXML.createNewFile();
        }
        var fileWriter = new FileWriter(fileXML, 'UTF-8');
        var xsw = new XmlStreamWriter(fileWriter);
        xsw.writeStartDocument('UTF-8', '1.0');
        xsw.writeStartElement('catalog');
        xsw.writeDefaultNamespace('http://www.demandware.com/xml/impex/catalog/2006-10-31');
        xsw.writeAttribute('catalog-id', siteCatalogID);
        while (productsIterator.hasNext()) {
            var product = productsIterator.next();
            xsw.writeStartElement('category-assignment');
            xsw.writeAttribute('category-id', categoryID);
            xsw.writeAttribute('product-id', product.productID);
            xsw.writeEndElement();
        }
        xsw.writeEndElement(); // catalog
        xsw.close();
        fileWriter.close();
    } catch (e) {
        return new Status(Status.ERROR, 'ERROR', e.message);
    }
    return new Status(Status.OK);
}

/**
 * creates catalog xml file for import with category assiggnments
 * for products that have specified brand attribute value
 * @param {Object} params - job's parameters
 * @returns {dw.system.Status} status
 */
function execute(params) {
    var brandName = params.brandName;
    var categoryID = params.categoryID;
    var workingFolder = params.workingFolder;
    var products = getProducts(brandName);

    if (products.size() > 0) {
        Logger.info(StringUtils.format('{0} products were found. Starting catalog xml file generation', products.size()));
        var status = createCategoryAssignmentFile(products, categoryID, workingFolder);

        if (status.error) {
            Logger.error(StringUtils.format('Error occurred during xml file generation. Error Message: "{0}"', status.getMessage()));
            return new Status(Status.ERROR);
        }
        Logger.info('Catalog xml file was successfully created under the working directory');
        return new Status(Status.OK);
    }
    Logger.info('Skipping the step. No products were found for specified brand value');
    return new Status(
        Status.OK,
        'NO_PRODUCTS_FOUND',
        StringUtils.format('Skipping the step. No products were found for specified brand value "{0}"', brandName)
    );
}

module.exports = {
    execute: execute
};
